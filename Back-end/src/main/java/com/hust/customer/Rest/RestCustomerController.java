package com.hust.customer.Rest;

import com.hust.customer.entity.Customer;
import com.hust.customer.DTO.CustomerRequest;
import com.hust.customer.exception.CustomErrorType;
import com.hust.customer.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/customer")
public class RestCustomerController {
    public static final Logger logger = LoggerFactory.getLogger(RestCustomerController.class);
    @Autowired
    private CustomerService customerService;

    @PreAuthorize("hasRole('ROLE_CUSTOMER_ADMIN')")
    @GetMapping("/")
    public ResponseEntity<List<Customer>> listAllCustomer() {
        List<Customer> customer = customerService.findAll();
        if (customer.isEmpty()) {
            return new ResponseEntity<List<Customer>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Customer>>(customer, HttpStatus.OK);
    }

    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Customer> createCustomer(@Valid @RequestBody final CustomerRequest customer) {
        logger.info("Create user:{}",customer);
        if(customer.getId()!=null)
            if (customerService.findById(customer.getId()).isPresent()) {
                return new ResponseEntity<Customer>(
                        new CustomErrorType(
                                "Unable to create new customer.A Customer with id already exist."),
                        HttpStatus.NOT_FOUND);
            }
        Customer response=this.customerService.save(new Customer(customer));
        return new ResponseEntity<Customer>(response, HttpStatus.CREATED);
    }
    @PreAuthorize("hasRole('ROLE_CUSTOMER_ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<Customer> getUserById(@PathVariable("id") final Long id) {
        Customer customer = customerService.findById(id).get();
        if (customer == null) {
            return new ResponseEntity<Customer>(new CustomErrorType("Customer with id " + id + " not found"),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Customer>(customer, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Customer> updateUser(@PathVariable("id") final Long id, @RequestBody CustomerRequest dto) {
        // fetch user based on id and set it to currentUser object of type UserDTO
        Optional<Customer> customerDTO = customerService.findById(id);
        if (!customerDTO.isPresent()) {
            return new ResponseEntity<Customer>(new CustomErrorType("Unable to update. Customer with id "+id+" not found"),HttpStatus.NOT_FOUND);
        }
        Customer data=customerDTO.get();
        // update currentUser object data with user object data
        data.setName(dto.getName());
        data.setAge(dto.getAge());
        data.setGender(dto.getGender());
        data.setBirthDate(dto.getBirthDate());
        data.setAddress(dto.getAddress());
        data.setPhone(dto.getPhone());
        data.setEmail(dto.getEmail());
        Customer response=customerService.save(data);
        return new ResponseEntity<Customer>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Customer> deleteUser(@PathVariable("id") final Long id) {
        Optional<Customer> customerDTO=this.customerService.findById(id);
        if (!customerDTO.isPresent()) {
            return new ResponseEntity<Customer>(new CustomErrorType("Unable to delete. Customer with id "+id+" not found."),HttpStatus.NOT_FOUND);
        }
        this.customerService.deleteCustomer(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(true);
        binder.registerCustomEditor(java.util.Date.class, new CustomDateEditor(sdf, true));
    }
}
