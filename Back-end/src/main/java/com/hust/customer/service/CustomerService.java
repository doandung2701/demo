package com.hust.customer.service;
import com.hust.customer.entity.Customer;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface CustomerService {
	Optional<Customer> findById(Long id);
	List<Customer> findAll();
	List<Customer> findAllWithPaging(Pageable pageable);
	Long count();
	Customer save(Customer user);
	void deleteCustomer(Long id);

}
