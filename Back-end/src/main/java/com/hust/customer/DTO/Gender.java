package com.hust.customer.DTO;

public enum Gender {
    MALE, FEMALE
}