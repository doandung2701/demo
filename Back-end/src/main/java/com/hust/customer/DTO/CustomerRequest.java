package com.hust.customer.DTO;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Date;

public class CustomerRequest implements Serializable {
    private Long id;
    @NotNull(message = "{error.name.null}")
    @NotEmpty(message = "{error.name.empty}")
    @Length(max = 50, message = "{error.name.length}")
    @Column(name = "NAME")
    private String name;

    @NotNull(message = "{error.age.null}")
    @Digits(fraction = 0, integer = 10, message ="Wrong format")
    @Column(name = "AGE")
    private Integer age;
    @NotNull(message = "{error.gender.null}")
    @Enumerated(EnumType.STRING)
    @Column(name = "GENDER")
    private Gender gender;
    @NotNull(message = "{error.birthDate.null}")
    @Temporal(TemporalType.DATE)
    @Column(name = "BIRTHDATE")
    private Date birthDate;

    @NotEmpty(message = "{error.address.empty}")
    @Length(max = 150, message = "{error.address.length}")
    @Column(name = "ADDRESS")
    private String address;

    @NotNull
    @NotEmpty(message = "{error.phone.empty}")
    @Pattern(regexp="(^$|[0-9]{10})",message = "{error.phone.phone}")
    @Column(name = "PHONE")
    private String phone;

    @NotNull
    @Email(message = "{error.email.email}")
    @NotEmpty(message = "{error.email.empty}")
    @Column(name = "EMAIL")
    private String email;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
