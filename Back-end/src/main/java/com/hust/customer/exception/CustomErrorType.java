package com.hust.customer.exception;


import com.hust.customer.entity.Customer;

public class CustomErrorType extends Customer {
	private String errorMessage;

	public CustomErrorType(final String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
}
