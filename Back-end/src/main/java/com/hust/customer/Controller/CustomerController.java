package com.hust.customer.Controller;

import com.hust.customer.entity.Customer;
import com.hust.customer.DTO.CustomerRequest;
import com.hust.customer.Rest.RestCustomerController;
import com.hust.customer.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("customers")
public class CustomerController {
    public static final Logger logger = LoggerFactory.getLogger(RestCustomerController.class);
    @Autowired
    private CustomerService customerService;


    @GetMapping("/index")
    public String listAllCustomer(Model model) {
        List<Customer> customers = customerService.findAll();
        model.addAttribute("customers",customers);
        return "index";
    }
    @GetMapping("delete/{id}")
    public String deleteCustomer(@PathVariable("id") Long id,Model model) {
        Optional<Customer> customerDTO=customerService.findById(id);
        if(customerDTO.isPresent()){
            customerService.deleteCustomer(id);
            return "redirect:/customers/index";
        }
        model.addAttribute("errorDetail","Customer not found");
        return "error";
    }
    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
          Customer editCustomer = customerService.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid customer Id:" + id));
        model.addAttribute("editCustomer", editCustomer);
        return "update-customer";
    }
    @PostMapping("/update")
    public String updateCustomer(@Valid @ModelAttribute("editCustomer") Customer editCustomer,
                             BindingResult result) {
        if (result.hasErrors()) {
            return "update-customer";
        }
        customerService.save(editCustomer);
        return "redirect:index";
    }
    @GetMapping("/add")
    public String addCustomerForm(Model modelAttribute,CustomerRequest customer) {
        if(customer==null){
            customer=new CustomerRequest();
        }
        modelAttribute.addAttribute("customer",customer);
        return "add-customer";
    }

    @PostMapping("/addcustomer")
    public String addUser(@Valid @ModelAttribute("customer") CustomerRequest customer, BindingResult result) {
        if (result.hasErrors()) {
            return "add-customer";
        }
        customerService.save(new Customer(customer));
        return "redirect:index";
    }
}
