import React, { Component } from 'react';
import { Modal, Spin, Button, Form, Input, Select, Upload,DatePicker, message } from 'antd';
import { InputNumber } from 'antd';
import moment from 'moment';

import Axios from 'axios';
const dateFormat = 'YYYY-MM-DD';

const FormItem = Form.Item;
const Option = Select.Option;

export default class CustomerModal extends Component {

    constructor(props) {
        super(props);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.isFormInvalid = this.isFormInvalid.bind(this);
        this.validateName = this.validateName.bind(this);
        this.validateAge = this.validateAge.bind(this);
        this.validatebirthDate = this.validatebirthDate.bind(this);
        this.validateAddress = this.validateAddress.bind(this);
        this.validatephone = this.validatephone.bind(this);
        this.validateemail = this.validateemail.bind(this);
        this.state = {
            id: 0,
            name: {
                value: ''
            },
            age: {
                value: 0,
            },
            gender: {
                value: 'MALE'
            },
            birthDate: {
                value: ''
            },
            address: {
                value: ''
            },
            phone: {
                value: ''
            },
            email: {
                value:''
            },
            loading:false,
        }
    }
    validateAge(age) {
        if (age < 0) {
            return {
                validateStatus: 'error',
                errorMsg: `Age need greater than 0`
            }
        } else if (!new RegExp('^[0-9]+$').test(age)) {
            return {
                validationStatus: 'error',
                errorMsg: `Age not using format`
            }
        } else {
            return {
                validateStatus: 'success',
                errorMsg: null,
            }
        }

    }

    validateName(name) {
        if (name.length < 3) {
            return {
                validateStatus: 'error',
                errorMsg: `Name is too short (Minimum 3 characters needed.)`
            }
        } else if (name.length > 50) {
            return {
                validationStatus: 'error',
                errorMsg: `Name is too long (Maximum 40 characters allowed.)`
            }
        } else {
            return {
                validateStatus: 'success',
                errorMsg: null,
            }
        }

    }
    validatebirthDate(date) {
        var patt=new RegExp('^(19[5-9][0-9]|20[0-4][0-9]|2050)[-/](0?[1-9]|1[0-2])[-/](0?[1-9]|[12][0-9]|3[01])$');
        if (date == '' || date == null) {
            return {
                validateStatus: 'error',
                errorMsg: `Date is required`
            }
        }
        if(!patt.test(date)){
            return {
                validateStatus: 'error',
                errorMsg: `Date format`
            }
        }
        return {
            validateStatus: 'success',
            errorMsg: null,
        }
    }

    validateAddress(address) {
        if (address.length < 3) {
            return {
                validateStatus: 'error',
                errorMsg: `Address is too short (Minimum 3 characters needed.)`
            }
        } else if (address.length > 150) {
            return {
                validationStatus: 'error',
                errorMsg: `Address is too long (Maximum 150 characters allowed.)`
            }
        } else {
            return {
                validateStatus: 'success',
                errorMsg: null,
            }
        }

    }

    validatephone(phone) {
        var phoneno = /^\d{10}$/;
        if (phone == '' || phone == null) {
            return {
                validateStatus: 'error',
                errorMsg: `Phone is required`
            }
        } else if (!phone.match(phoneno)) {
            return {
                validationStatus: 'error',
                errorMsg: `Phone not correct`
            }
        } else {
            return {
                validateStatus: 'success',
                errorMsg: null,
            }
        }

    }

    validateemail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (email == '' || email == null) {
            return {
                validateStatus: 'error',
                errorMsg: `email is required`
            }
        } else if (!re.test(String(email).toLowerCase())) {
            return {
                validationStatus: 'error',
                errorMsg: `Email not using format`
            }
        } else {
            return {
                validateStatus: 'success',
                errorMsg: null,
            }
        }

    }
    handleInputChange(event, validationFun) {
        const target = event.target;
        const inputName = target.name;
        const inputValue = target.value;
        console.log(inputName);
       
            this.setState({
                [inputName]: {
                    value: inputValue,
                    ...validationFun(inputValue)
                }
            });

    }
    isFormInvalid() {
        return !(this.state.age.validateStatus === 'success' &&
            this.state.name.validateStatus === 'success'
            && this.state.address.validateStatus === 'success'
            && this.state.phone.validateStatus === 'success'
            && this.state.email.validateStatus === 'success'
        );
    }
    handleOk = () => {
        let {name,age,gender,birthDate,address,phone,email}=this.state;
        let payload={
            name:name.value,
            age:age.value,
            gender:gender.value,
            birthDate:birthDate.value,
            address:address.value,
            phone:phone.value,
            email:email.value
        }
        this.setState({
            loading:true
        })
        console.log(this.payload);
        
        if(this.props.idEdit==0){
            Axios.post("http://localhost:8080/api/customer/",payload).then(data=>{
                this.props.addCustomer(data.data);
                this.handleCancel();
            }).catch(e=>{
                console.log(e);
                
            })
        }else{
            Axios.put(`http://localhost:8080/api/customer/${this.props.idEdit}`,payload).then(data=>{
                console.log("Edit",data.data);
                
                this.props.EditCustomer(data.data);
                this.handleCancel();
            }).catch(e=>{
                console.log(e);
                
            })
        }
        
        this.setState({
            loading:false
        })
        // let {authorName,thumbnail,briefDescription}=this.state;
        // const {authorId}=this.props.authorModal.author;
        // console.log(this.props.authorModal.author);
        // if(authorId==0){ //undefind == 0
        //     var payload={
        //         authorName:authorName.value,
        //         thumbnail: thumbnail.value,
        //         briefDescription: briefDescription.value
        //     }
        //     console.log(payload);
        //     this.props.createAuthor(payload);
        // }else{
        //     var payload={
        //         authorId: authorId,
        //         authorName:authorName.value,
        //         thumbnail: thumbnail.value,
        //         briefDescription: briefDescription.value
        //     }
        //     console.log(payload);
        //     this.props.updateAuthor(payload);
        // }
        // this.handleCancel();
    }
    handleCancel = () => {

        this.state = {
            id: 0,
            name: {
                value: ''
            },
            age: {
                value: 0,
            },
            gender: {
                value: 'MALE'
            },
            birthDate: {
                value: ''
            },
            address: {
                value: ''
            },
            phone: {
                value: ''
            },
            email: {
                value:''
            },
            loading:false,
           
        }
        this.props.closeModal();
    }
    hanleSelectChange=event=>{
        this.setState({
            gender: {
                value: event
            },
        })
        
    }
    handleChangeDate=(data,dateString,validationFun)=>{
           this.setState({
            birthDate: {
                value: dateString,
                ...validationFun(dateString)
            },
           })
            
    }
    
    handleAgeChange=(event,validationFun)=>{
        this.setState({
            age: {
                value: event,
                ...validationFun(event)
            }
        })
    }
    render() {
        let {defaultValue}=this.props;
        console.log(defaultValue);
        
        return <div>
            <Spin spinning={this.state.loading}>
                <Modal
                   
                //title={this.props.authorModal.author.id == 0 ? "Create New Author" : "Edit Author"}
                 visible={this.props.isShow}
                 onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                    <Button key="back" onClick={this.handleCancel}>Return</Button>,
                    <Button
                        disabled={this.isFormInvalid()}
                        key="submit" type="primary" onClick={this.handleOk}>
                        Submit
                    </Button>,
                ]}
                >
                    <Form
                    >
                        <FormItem
                            label="Customer Name"
                            validateStatus={this.state.name.validateStatus}
                            help={this.state.name.errorMsg}>
                            <Input
                                 defaultValue={defaultValue.name}
                                size="large"
                                name="name"
                                placeholder="Customer name"
                                onChange={(event) => this.handleInputChange(event, this.validateName)} />
                        </FormItem>
                        <FormItem
                            label="Customer Age"
                            validateStatus={this.state.age.validateStatus}
                            help={this.state.age.errorMsg}>
                            <InputNumber
                                 defaultValue={defaultValue.age}
                                size="large"
                                name="age"
                                placeholder="Customer age"
                                onChange={(event) => this.handleAgeChange(event, this.validateAge)} />
                        </FormItem>
                        <FormItem
                            label="Customer gender"
                            validateStatus={this.state.gender.validateStatus}
                            help={this.state.gender.errorMsg}>
                            <Select name="gender" style={{ width: 120 }} defaultValue={defaultValue.gender}
                            onChange={(event) => this.hanleSelectChange(event)} >
                                <Option value="MALE">MALE</Option>
                                <Option value="FEMALE">FEMALE</Option>
                            </Select>
                        </FormItem>
                        <FormItem
                            label="Customer birthDate"
                            validateStatus={this.state.birthDate.validateStatus}
                            help={this.state.birthDate.errorMsg}>
                             <DatePicker  format={dateFormat}  name="birthDate"

                        defaultValue={moment(defaultValue.birthDate, dateFormat)} format={dateFormat}
                              onChange={(event,dateString) => this.handleChangeDate(event,dateString, this.validatebirthDate)}/>
                           
                        </FormItem>


                        <FormItem
                            label="Customer Address"
                            validateStatus={this.state.address.validateStatus}
                            help={this.state.address.errorMsg}>
                            <Input
                                defaultValue={defaultValue.address}
                                size="large"
                                name="address"
                                placeholder="Customer address"
                                onChange={(event) => this.handleInputChange(event, this.validateAddress)} />
                        </FormItem>

                        <FormItem
                            label="Customer Phone"
                            validateStatus={this.state.phone.validateStatus}
                            help={this.state.phone.errorMsg}>
                            <Input
                        defaultValue={defaultValue.phone}
                        size="large"
                                name="phone"
                                placeholder="Customer phone"
                                onChange={(event) => this.handleInputChange(event, this.validatephone)} />
                        </FormItem>

                        <FormItem
                            label="Customer Email"
                            validateStatus={this.state.email.validateStatus}
                            help={this.state.email.errorMsg}>
                            <Input
                               defaultValue={defaultValue.email}
                                size="large"
                                name="email"
                                placeholder="Customer email"
                                onChange={(event) => this.handleInputChange(event, this.validateemail)} />
                        </FormItem>
                    </Form>
                </Modal >
            </Spin>
        </div>

    }
}


