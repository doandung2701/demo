import React from 'react';
import { Table, Popconfirm, Divider, Button, Input, Icon, message } from 'antd';
import Highlighter from 'react-highlight-words';
import { DatePicker } from 'antd';
import moment from 'moment';
import Axios from 'axios';
import CustomerModal from './CustomerModal';
const dateFormat = 'YYYY-MM-DD';

class App extends React.Component {

  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
        setSelectedKeys, selectedKeys, confirm, clearFilters,
    }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => { this.searchInput = node; }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => this.handleSearch(selectedKeys, confirm)}
                    icon="search"
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    Search
        </Button>
                <Button
                    onClick={() => this.handleReset(clearFilters)}
                    size="small"
                    style={{ width: 90 }}
                >
                    Reset
        </Button>
            </div>
        ),
    filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
        if (visible) {
            setTimeout(() => this.searchInput.select());
        }
    },
    render: (text) => (
        <Highlighter
            highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
            searchWords={[this.state.searchText]}
            autoEscape
            textToHighlight={text}
        />
    ),
})
handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
}
handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: '' });
}
constructor(props) {
    super(props);
    this.state={
      customerList:[],
      isLoading:true,
      showModal:false,
      idEdit:null,
      defaultValue:{
        name: '',
        age: 0,
        gender:'MALE',
        birthDate: '2019-01-01',
        address: '',
        phone: '',
        email:'',
    }
    }
    this.columns = [
        {
            title: 'Customer ID',
            dataIndex: 'id',
            key: 'id',
        }, {
            title: 'Customer name',
            dataIndex: 'name',
            key: 'name',
            onFilter: (value, record) => record.name.indexOf(value) === 0,
            sorter: (a, b) => a.name.length - b.name.length,
            sortDirections: ['descend', 'ascend'],
            ...this.getColumnSearchProps('name'),
            // sorter: (a, b) => a.value - b.value,
        },
        {
            title: 'Customer age',
            dataIndex: 'age',
            key: 'age',
            onFilter: (value, record) => record.age.indexOf(value) === 0,
            sorter: (a, b) => a.age - b.age,
            sortDirections: ['descend', 'ascend'],
            //...this.getColumnSearchProps('age'),
            // sorter: (a, b) => a.value - b.value,
        },
        {
            title: 'gender',
            dataIndex: 'gender',
            key: 'gender',
            onFilter: (value, record) => record.gender.indexOf(value) === 0,
            sorter: (a, b) => a.gender.length - b.gender.length,
            sortDirections: ['descend', 'ascend'],
            ...this.getColumnSearchProps('gender'),
                        // sorter: (a, b) => a.value - b.value,
        },
        {
          title: 'birthDate',
          dataIndex: 'birthDate',
          key: 'birthDate',
          render:text=>
          <DatePicker value={moment(text, dateFormat)} format={dateFormat} disabled />
      },
      , {
        title: 'Customer address',
        dataIndex: 'address',
        key: 'address',
        onFilter: (value, record) => record.address.indexOf(value) === 0,
        sorter: (a, b) => a.address.length - b.address.length,
        sortDirections: ['descend', 'ascend'],
        ...this.getColumnSearchProps('address'),
        // sorter: (a, b) => a.value - b.value,
    },
    , {
      title: 'Customer phone',
      dataIndex: 'phone',
      key: 'phone',
      onFilter: (value, record) => record.phone.indexOf(value) === 0,
      sorter: (a, b) => a.phone.length - b.phone.length,
      sortDirections: ['descend', 'ascend'],
      ...this.getColumnSearchProps('phone'),
      // sorter: (a, b) => a.value - b.value,
  },
  , {
    title: 'Customer email',
    dataIndex: 'email',
    key: 'email',
    onFilter: (value, record) => record.email.indexOf(value) === 0,
    sorter: (a, b) => a.email.length - b.email.length,
    sortDirections: ['descend', 'ascend'],
    ...this.getColumnSearchProps('email'),
    // sorter: (a, b) => a.value - b.value,
},
        {
            title: 'operation',
            dataIndex: 'operation',
            render: (text, record) => (
                <span>
                    {this.state.customerList.length >= 1
                        ? (
                            <Popconfirm title="Sure to delete?" onConfirm={() => this.delete(record.id)}>
                                <a href="javascript:;">Delete</a>
                            </Popconfirm>
                        ) : null}
                        <Divider type="vertical" />
                        <Button type="primary" onClick={() => {
                            this.openModal(record.id) // sua lai cho nay
                        }}>Edit</Button>
                </span>
            ),
        }
    ]
  
}
EditCustomer= async(customer)=>{
  let list=this.state.customerList;
  let index=list.findIndex(data=>data.id==customer.id);
  list[index]=customer;
  await this.setState({
      customerList:list,
      isLoading:false,
  })
  message.success("Update succeess")
}
delete=(id)=>{
    this.setState({
      isLoading:true,
    })
    Axios.delete(`http://localhost:8080/api/customer/${id}`).then(data=>{
      let list=this.state.customerList;
      this.setState({
          customerList:list.filter(data=>data.id!=id),
          isLoading:false,
      })
      message.success("Delete success");
    }).catch(e=>{
      message.error("delete error");
      this.setState({
        isLoading:false,
      })
    })
}
openModal=async (id)=>{
  if(id!=0 ){
    await  Axios.get(`http://localhost:8080/api/customer/${id}`).then(data=>{
      let body=data.data;
      console.log(body);
      
      this.setState({
          showModal:true,
         defaultValue:body,
         idEdit:id
      })
  }).catch(e=>{
      message.error("Has error when load data")
      this.setState({
          loading:false
      })
  })
  }

 else 
 await this.setState({
  idEdit:0,
    showModal:true,
    defaultValue:{
      name: '',
      age: 0,
      gender:'MALE',
      birthDate: '2019-01-01',
      address: '',
      phone: '',
      email:'',
    }
  })
}
addCustomer=(data)=>{
  this.setState({
    customerList:[...this.state.customerList,data]
  })
  message.success("Create success");
}
componentDidMount() {
  Axios.get("http://localhost:8080/api/customer/").then(data=>{
    console.log(data.data);
    this.setState({
      customerList:data.data,
      isLoading:false
    })
  })

}
closeModal=()=>{
  this.setState({
    showModal:false,
    idEdit:0,
    defaultValue:{
      name: '',
      age: 0,
      gender:'MALE',
      birthDate: '2019-01-01',
      address: '',
      phone: '',
      email:'',
    }
  })
}
render() {
    const data = this.state.customerList.map((data, index) => (
        {
            id: data.id,
            name: data.name,
            age: data.age,
            gender: data.gender,
            birthDate: data.birthDate,
            address: data.address,
            phone: data.phone,
            email: data.email,
        }
    ))
    return (
        <div>
                <Button type="primary" onClick={() => this.openModal(0)}>Create</Button>

            <div>
                <Table columns={this.columns} dataSource={data}
                    rowKey={record => record.id}
                    pagination={{ pageSize: 7 }}
                    loading={this.state.isLoading}
                />
                {this.state.showModal&&<CustomerModal defaultValue={this.state.defaultValue} idEdit={this.state.idEdit}  isShow={this.state.showModal} closeModal={this.closeModal} addCustomer={this.addCustomer}
                EditCustomer={this.EditCustomer}
                
                />}
            </div>
        </div>
    )
}
}

export default App;
